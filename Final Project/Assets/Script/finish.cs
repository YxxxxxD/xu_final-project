﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class finish : MonoBehaviour
{
    // Start is called before the first frame update
    private void Start()
    {
        GameObject canvas = GameObject.Find("Canvas");
        canvas.transform.Find("Panel").gameObject.SetActive(false);
        
    }

  

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            GameObject canvas = GameObject.Find("Canvas");
            canvas.transform.Find("Panel").gameObject.SetActive(true);
            Time.timeScale = 0;
        }
    }
}
