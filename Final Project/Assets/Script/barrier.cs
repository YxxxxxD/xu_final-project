﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;

public class barrier : MonoBehaviour
{
    // Start is called before the first frame update
    private void Start()
    {
        GameObject canvas = GameObject.Find("Canvas");
        canvas.transform.Find("Panel3").gameObject.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
      
        if (other.name =="Player")
        {
            Time.timeScale = 0; }
        GameObject canvas = GameObject.Find("Canvas");
        canvas.transform.Find("Panel3").gameObject.SetActive(true);
    }
}