﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Threading;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Security.AccessControl;

public class Player : MonoBehaviour
{


    public float speed;
    public float turnspeed;
    public Vector3 jump;
    public float jumpForce = 2.0f;
    public bool isGrounded;
    Rigidbody rb;
    public Text countText;
    private int count;

    public GameObject prefabMusic;
    

    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 0;
        count = 0;
        SetCountText();
        rb = GetComponent<Rigidbody>();
        jump = new Vector3(0.0f, 5.0f, 0.0f);
        speed = 10;


        var music = GameObject.Find("BgMusic");
        if (music == null)
        {
            var m = Instantiate(prefabMusic, null);
            m.name = "BgMusic";
            DontDestroyOnLoad(m);
        }   
        
       
      
    }

    void OnCollisionStay()
    {
        isGrounded = true;
    }




    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
        {

            rb.AddForce(jump * jumpForce, ForceMode.Impulse);
            isGrounded = false;
        }

        float x = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
        transform.Translate(x * turnspeed * Time.deltaTime, 0, speed * Time.deltaTime);

        var c = Camera.main.transform;
        Quaternion cur = c.rotation;
        Quaternion target = cur * Quaternion.Euler(0, 0, x * 1.5f);
        Camera.main.transform.rotation = Quaternion.Slerp(cur, target, 0.5f);


        
        

        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene("Main");
            Time.timeScale = 1;

            return;
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            GameObject canvas = GameObject.Find("Canvas");
            canvas.transform.Find("Panel4").gameObject.SetActive(false);
            Time.timeScale = 1;
        }
        if (Input.GetKeyDown(KeyCode.Escape)) 
        { 
            Application.Quit(); 
        }

    }
    
    private void OnTriggerEnter(Collider other)
    {
        

        if (other.gameObject.CompareTag("Accelerator"))
        {
            speed = speed + 5;
        }

        if (other.gameObject.CompareTag("Coin"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            SetCountText();
        }

        if (other.gameObject.CompareTag("Obstacle"))
        {
            speed = 0;
        }



    }
    void SetCountText()
    {
        countText.text = "Scores: " + count.ToString();

    }
}

